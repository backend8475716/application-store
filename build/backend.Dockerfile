FROM composer:2.7.2 AS composer
FROM mlocati/php-extension-installer:2.2.5 AS extension-installer
FROM php:8.3.4-alpine3.19 AS base

ENV TZ=UTC
ARG GID=1000
ARG UID=1000
ARG ENVIRONMENT_NAME=local

ARG ALPINE_PACKAGES='nodejs npm'
ARG PHP_EXENSIONS='pcntl bcmath zip gd exif pgsql pdo_pgsql sockets redis openswoole'
ENV NODE_PATH '/home/$UID/.npm-global/lib/node_modules'

COPY ./php.ini-production $PHP_INI_DIR/php.ini
COPY --from=extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone;

RUN if getent passwd noroot; then deluser noroot; fi; \
    if getent group noroot; then delgroup noroot; fi; \
    addgroup -g $GID --system noroot && adduser -s noroot -G noroot -D -u $UID noroot;

RUN set -ex; \
    IPE_PROCESSOR_COUNT=$(nproc) IPE_GD_WITHOUTAVIF=1 install-php-extensions $PHP_EXENSIONS; \
    \
    if [ $ENVIRONMENT_NAME = 'local' ]; then \
        apk add --no-cache $ALPINE_PACKAGES; \
        mkdir -p /home/$UID/.npm-global/; \
        npm config set prefix '/home/$UID/.npm-global/'; \
        npm install -g chokidar; \
        chown -R $UID:$GID /home/$UID/.npm-global/; \
        npm cache clean --force; \
    fi; \
    apk cache clean && docker-php-source delete && rm -rf /usr/local/bin/install-php-extensions;

USER $UID

WORKDIR /app

COPY --chown=$UID:$GID --from=composer /usr/bin/composer /usr/bin/composer
COPY --chown=$UID:$GID . ./

HEALTHCHECK --start-period=20s --interval=10s --timeout=5s --retries=3 CMD php artisan octane:status || exit 1

FROM base as server

ARG DEBUG=true

RUN composer install \
      --no-interaction \
      --no-progress \
      --no-autoloader \
      --no-cache \
      --ansi \
      --no-scripts \
      $(if [ $DEBUG == "false" ]; then echo '--no-dev'; fi); \
    composer dump-autoload --optimize --strict-psr --apcu;

CMD ["sh", "-c", "php artisan migrate --force --seed && php artisan octane:start --host=0.0.0.0 --port=8080 --workers=2 --task-workers=2 --max-requests=10000"]

FROM base as server-local

ENTRYPOINT ["/entrypoint.sh"]

FROM nginxinc/nginx-unprivileged:1.24-perl as nginx

ARG UID=1000
ARG GID=1000

RUN rm -rf /etc/nginx/conf.d/default.conf
COPY --chown=$UID:$GID ./nginx.conf /etc/nginx/conf.d/laravel_server_block.conf

USER $UID

WORKDIR /app/public

COPY --chown=$UID:$GID --from=base /app/public /app/public
