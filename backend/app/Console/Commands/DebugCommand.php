<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;

class DebugCommand extends Command
{
    protected $signature = 'debug';

    public function handle()
    {

        Storage::disk()->put('text.txt', 'ddd');
        dump(Storage::url('text.txt'));
    }
}