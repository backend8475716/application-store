<?php

use App\Http\Controllers\CreateApplicationController;
use App\Http\Controllers\CreateApplicationVersionController;
use App\Http\Controllers\DeleteApplicationController;
use App\Http\Controllers\DeleteApplicationVersionController;
use App\Http\Controllers\EditApplicationController;
use App\Http\Controllers\EditApplicationVersionController;
use App\Http\Controllers\ListApplicationController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ShowApplicationController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum', 'ability:read')->group(function () {
    Route::prefix('applications')->group(function () {
        Route::get('/', ListApplicationController::class)->name('list-applications');
        Route::get('{application}', ShowApplicationController::class)->name('show-application');
    });
});

Route::middleware('auth:sanctum', 'ability:write')->group(function () {
    Route::prefix('applications')->group(function () {
        Route::post('create', CreateApplicationController::class)->name('create-application');
        Route::put('{application}', EditApplicationController::class)->name('edit-application');
        Route::delete('{application}', DeleteApplicationController::class)->name('delete-application');
    });

    Route::prefix('application-versions')->group(function () {
        Route::post('create', CreateApplicationVersionController::class)->name('create-application-versions');
        Route::put('{applicationVersion}', EditApplicationVersionController::class)->name('edit-application-versions');
        Route::delete('{applicationVersion}', DeleteApplicationVersionController::class)->name('delete-application-versions');
    });
});

Route::post('/login', LoginController::class)->name('login');
