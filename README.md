# application store

## Запуск проекта

1. Создать файлы с переменными окружения
 - Для backend-сервиса: скопировать `./backend/.env.example` в новый файл `./backend/.env`
 - Для docker-compose: скопировать `.env.example` в новый файл `.env`

2. Выполнить команду `docker compose up -d` для запуска контейнеров проекта.

3. Убедиться в том, что все контейнеры успешно запущены: `docker compose ps`

## Работа с backend-сервисом в консоли

1. При необходимости выполнить консольную команду в backend-сервисе:
 - когда сервис уже запущен:
   `docker compose exec backend <COMMAND_NAME> <COMMAND_ARGUMENTS>`
 - когда необходимо запустить backend-сервис без зависимостей:
   `docker compose run --rm --no-deps backend <COMMAND_NAME> <COMMAND_ARGUMENTS>`
 - когда необходимо запустить bakend-сервис вместе с зависимостями (база данных, redis)
   `docker compose run --rm backend <COMMAND_NAME> <COMMAND_ARGUMENTS>`

## Вспомогательные команды

> Все `docker compose` команды необходимо запускать из корня проекта
>
> Синтаксис `docker-compose` в текущий момент переходит в устаревший, лучше использовать написание через пробел

### Пересборка сервиса без кеша

```shell
docker compose --progress=plain build --no-cache backend
```

### Просмотр результирующего файла конфигурации docker-compose по отдельному сервису

```shell
docker compose config backend
```
