<?php

namespace Database\Factories;

use App\Models\Application;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Application>
 */
class ApplicationVersionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'number' => $this->faker->name(),
            'description' => $this->faker->realText(),
            'application_id' => Application::factory(),
            'url' => $this->faker->url(),
            'is_active' => $this->faker->boolean(),
        ];
    }
}
