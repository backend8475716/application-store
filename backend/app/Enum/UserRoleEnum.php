<?php

namespace App\Enum;

enum UserRoleEnum: string
{
    case DEVELOPER = 'developer';
    case TESTER = 'tester';

    public function getAbilities(): array
    {
        return match ($this) {
            self::DEVELOPER => ['read', 'write'],
            self::TESTER => ['read'],
        };
    }
}