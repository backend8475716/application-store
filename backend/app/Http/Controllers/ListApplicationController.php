<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\ApplicationResource;
use App\Models\Application;
use Illuminate\Http\JsonResponse;

class ListApplicationController extends Controller
{
    public function __invoke(): JsonResponse
    {
        $applications = Application::query()->with(['activeApplicationVersions'])->orderBy('name')->get();

        return new JsonResponse([
            'data' => ApplicationResource::collection($applications)
        ], 200);
    }
}
