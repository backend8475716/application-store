up-d:
	docker compose up --detach

run-sh:
	docker compose run --rm --no-deps backend sh

build-no-cache:
	docker compose --progress=plain build --no-cache backend
