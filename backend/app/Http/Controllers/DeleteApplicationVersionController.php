<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\ApplicationVersion;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteApplicationVersionController extends Controller
{
    public function __invoke(ApplicationVersion $applicationVersion): JsonResponse
    {
        $applicationVersion->delete();
        return new JsonResponse([], Response::HTTP_OK);
    }
}
