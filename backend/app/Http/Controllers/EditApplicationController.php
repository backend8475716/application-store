<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\EditApplicationRequest;
use App\Http\Resources\ApplicationResource;
use App\Models\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class EditApplicationController extends Controller
{
    public function __invoke(EditApplicationRequest $request, Application $application): JsonResponse
    {
        $application->update($request->toArray());
        return new JsonResponse([
            'data' => ApplicationResource::make($application)
        ], Response::HTTP_OK);
    }
}
