<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * @property string $number
 * @property string $description
 * @property int $application_id
 * @property string $url
 * @property bool $is_active
 */
class ApplicationVersion extends Model
{
    use HasFactory;

    protected $fillable = [
        'number',
        'description',
        'application_id',
        'url',
        'is_active'
    ];
}
