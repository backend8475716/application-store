<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteApplicationController extends Controller
{
    public function __invoke(Application $application): JsonResponse
    {
        $application->delete();
        return new JsonResponse([], Response::HTTP_OK);
    }
}
