<?php

namespace Database\Seeders;

use App\Models\Application;
use App\Models\ApplicationVersion;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public array $array = [];

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $userData = config('auth.seeders.user');
        if (!User::query()->where('email', $userData['email'])->exists()) {
            User::factory()->state([
                'email' => $userData['email'],
                'password' =>$userData['password']
            ])->create();
        }
        if (Application::query()->count() < 10) {
            $applications = Application::factory(10)->create();
            $applications->each(fn (Application $application) => ApplicationVersion::factory(10)->state([
                'application_id' => $application->id,
            ])->create());
        }
    }
}
