<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\EditApplicationVersionRequest;
use App\Http\Resources\ApplicationVersionResource;
use App\Models\ApplicationVersion;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class EditApplicationVersionController extends Controller
{
    public function __invoke(EditApplicationVersionRequest $request, ApplicationVersion $applicationVersion): JsonResponse
    {
        $applicationVersion->update($request->toArray());
        return new JsonResponse([
            'data' => ApplicationVersionResource::make($applicationVersion)
        ], Response::HTTP_OK);
    }
}
