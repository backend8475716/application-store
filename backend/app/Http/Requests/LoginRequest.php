<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|exists:users,email|email:rfc,dns',
            'password' => 'required|string|min:6',
        ];
    }

    public function user($guard = null): User
    {
        /** @var User $user */
        $user = User::query()->where('email', $this->input('email'))->first();

        return $user;
    }
}

