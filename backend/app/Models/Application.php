<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property name
 * @property description
 * @property icon_url
 */
class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'icon_url',
        'versions'
    ];

    public function applicationVersions(): HasMany
    {
        return $this->hasMany(ApplicationVersion::class);
    }

    public function activeApplicationVersions(): HasMany
    {
        return $this->hasMany(ApplicationVersion::class)->where('is_active', true);
    }
}
