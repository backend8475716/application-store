<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request): array
    {
        $user = $request->user();

        return [
            'token' => $user->createToken('name', $user->role->getAbilities())
        ];
    }
}

