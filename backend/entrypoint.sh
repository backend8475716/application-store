#!/bin/sh

if [ "${1}" != "" ]; then exec "$@"; fi

if [ ! -d "/app/vendor" ]; then
    echo "No such directory: /app/vendor. Installing composer packages" ;
    composer install --no-progress --ansi --no-scripts;
else
    echo "Nothing to install, update or remove: /app/vendor already exists";
fi

php artisan migrate --force;

exec php artisan octane:start --watch --host=0.0.0.0 --port=8080 --workers=2 --task-workers=2 --max-requests=10000 --poll;
