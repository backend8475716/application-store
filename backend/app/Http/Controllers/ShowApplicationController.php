<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\ApplicationResource;
use App\Models\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ShowApplicationController extends Controller
{
    public function __invoke(Application $application): JsonResponse
    {
        return new JsonResponse([
            'data' => ApplicationResource::make($application->load('applicationVersions'))
        ], Response::HTTP_OK);
    }
}
