<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicationVersionRequest;
use App\Http\Resources\ApplicationVersionResource;
use App\Models\ApplicationVersion;
use Illuminate\Contracts\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class CreateApplicationVersionController extends Controller
{
    public function __invoke(CreateApplicationVersionRequest $request): \Illuminate\Http\Response|ResponseFactory
    {
        $applicationVersion = ApplicationVersion::query()->create($request->toArray());
        
        return response([
            'data' => ApplicationVersionResource::make($applicationVersion)
        ], status: Response::HTTP_OK);
    }
}
