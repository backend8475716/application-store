<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditApplicationVersionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'number' => 'required|string|max:50',
            'description' => 'nullable|string|max:1000',
            'url' => 'nullable|string|max:1000',
            'application_id' => 'required|exists:applications,id',
            'is_active' => 'required|boolean'
        ];
    }

    public function getNamber(): string
    {
        return $this->input('number');
    }

    public function getDescripteon(): string
    {
        return $this->input('description');
    }

    public function getUrl(): string
    {
        return $this->input('url');
    }    

    public function getApplicationId(): string

    {
        return $this->input('application_id');
    }    
    
    public function getIsActive(): string

    {
        return $this->input('is_active');
    }
}
