<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicationRequest;
use App\Http\Resources\ApplicationResource;
use App\Models\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class CreateApplicationController extends Controller
{
    public function __invoke(CreateApplicationRequest $request): \Illuminate\Http\Response|ResponseFactory
    {
        $application = Application::query()->create($request->toArray());
        
        return response([
            'data' => ApplicationResource::make($application)
        ], status: Response::HTTP_OK);
    }
}
